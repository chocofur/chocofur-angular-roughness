# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Created by Chocofur, Styriam Sp. z o.o.

bl_info = {
    "name" : "Chocofur Angular Roughness",
    "author" : "Chocofur",
    "description" : "Add Angular Roughness effect to Cycles/EEVEE materials",
    "blender" : (2, 80, 0),
    "version" : (1, 1, 0),
    "location" : "Properties panel > Material",
    "category" : "Material"
}

from . import auto_load
from . import addon_updater_ops
from .addon_updater import Updater as updater

auto_load.init(ignore=("addon_updater", "addon_updater_ops"))

def register():
    # addon updater code and configurations
    # in case of broken version, try to register the updater first
    # so that users can revert back to a working version
    addon_updater_ops.register(bl_info)
    updater.engine = "bitbucket"
    updater.user = "pierog"
    updater.repo = "chocofur-angular-roughness"
    updater.addon = "chocofur_angular_roughness"
    updater.remove_pre_update_patterns = ["*.py","*.pyc"]

       
    auto_load.register()
    addon_updater_ops.check_for_update_background()

def unregister():
    auto_load.unregister()
    addon_updater_ops.unregister()
